module.exports = app => {
const users = require("../controllers/user.controller.js");
const auth = require('../../middleware/auth');

  // Register a new User
  app.post("/register", users.create);

  app.post("/login", users.auth);

  app.get("/user", auth, users.getuser);

};
