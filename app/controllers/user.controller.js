const User = require("../models/user.model.js");
const sql = require("../models/db.js");
var bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('config');

// Create and Save a new Customer
exports.create = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }

var userExists = false;
sql.query(`select * from users where email = '${req.body.email}' `, (err,rows) => {
    if(err) throw err;
    if(rows.length == 0){
      // Create a Customer
      const user = new User({
        email: req.body.email,
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        password: req.body.password
      });

      //encrypt the password with bcrypt
      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(user.password, salt, (err, hash) => {
          if(err) throw err;
          user.password = hash;
        });
      });

    //check if all fields have values

      // Save Customer in the database
    User.create(user, (err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while creating the User."
          });
        else{
          //create json access token
          const expiresIn = 24  *  60  *  60;
          const accessToken = jwt.sign({ id:  user.id }, config.get('jwtSecret'), {
              expiresIn: expiresIn
          });
          res.send({ "user": data, "access_token": accessToken, "expires_in": expiresIn
          });
        }
      });
    }
    else{
      return res.status(400).json({ msg: 'User already exists' });
    }

  });

};


exports.auth = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }


sql.query(`select * from users where email = '${req.body.email}' `, (err,rows) => {
    if(err) throw err;
    if(rows.length == 0){
      return res.status(400).json({ msg: 'User does not exist' });
    }
    else{
      const  email  =  req.body.email;
      const  password  =  req.body.password;
      //compare password with bcrypt
      //sign json web token
      const  result  =  bcrypt.compareSync(password, rows[0].password);
        if(!result) return  res.status(401).send('Password not valid!');

        const  expiresIn  =  24  *  60  *  60;
        const  accessToken  =  jwt.sign({ id:  rows[0].id }, config.get('jwtSecret'), {
            expiresIn:  expiresIn
        });
        res.status(200).send({ "user":  rows[0], "access_token":  accessToken, "expires_in":  expiresIn});

    }
  });

};


exports.getuser = (req, res) => {
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }


sql.query(`select * from users where id = '${req.body.id}' `, (err,rows) => {

  //make sure the id exists for a user
  const newObject = {
    id: rows[0].id,
    email: rows[0].email,
    firstname: rows[0].firstname,
    lastname: rows[0].lastname
  }
  res.send(newObject);
});

};
