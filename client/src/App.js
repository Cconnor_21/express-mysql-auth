import React, { Component} from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Example from "./components/Example.js";
import Register from "./components/Register.js";
import Login from "./components/Login.js";

class App extends Component{
  render(){
    return(
      <div className="App">
      <Router>
        <div>
          <Route exact path="/" component={Login} />
          <Route path="/register" component={Register} />
        </div>
      </Router>
      </div>
    );
  }
}

export default App;
