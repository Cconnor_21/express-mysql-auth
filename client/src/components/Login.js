import React, {useState, useEffect} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function Register(){
  return(
  <div className="jumbotron" >
    <h1 className="display-4">Login</h1>
    <p>If you don't have an account. <b><Link to="/register">Register Here</Link></b></p>
    <form>
    <div className="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
      <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div className="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
    </div>
  <button type="submit" className="btn btn-primary">Login</button>
</form>
  </div>
  );
}

export default Register;
