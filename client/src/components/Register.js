import React, {useState, useEffect} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


function Register(){
  return(
  <div className="jumbotron" >
    <h1 className="display-4">Register</h1>
    <p>If you already have an account. <b><Link to="/">Login Here</Link></b></p>
    <form>
    <div className="form-group">
      <label for="exampleInputEmail1">First Name</label>
      <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter First Name" />
    </div>
    <div className="form-group">
      <label for="exampleInputEmail1">Last Name</label>
      <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter Last Name" />
    </div>
    <div className="form-group">
      <label for="exampleInputEmail1">Email address</label>
      <input type="email" className="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
      <small id="emailHelp" className="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div className="form-group">
      <label for="exampleInputPassword1">Password</label>
      <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
    </div>
    <div className="form-group">
      <label for="exampleInputPassword1">Confirm Password</label>
      <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" />
    </div>
  <button type="submit" className="btn btn-primary">Submit</button>
</form>
  </div>
  );
}

export default Register;
